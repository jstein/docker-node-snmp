FROM node:0.10.40

COPY index.js /data/

CMD ["/data/index.js"]