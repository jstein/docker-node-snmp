#!/usr/bin/env node

var _ = require('lodash');

// This guy handles sending alerts in a nice, respectful way. Won't barrage
// you every five seconds. You can call sendAlerts() as often as you
// like, but we will only actually send every throttleMinutes per
// alert type.
function Alerter(mailer, throttleMinutes) {
  throttleMinutes = throttleMinutes || 60;
  this.threshhold = throttleMinutes * 60 * 1000; // one hour.
  this.mailer = mailer;
  this.throttle = {};
}

Alerter.prototype._send = function _send(hostname, key, info) {
  this.mailer.send('SYSTEM ALERT ' + hostname + ' ' + info.description,
    JSON.stringify({
      hostname: hostname,
      key: key,
      info: info
    }, null, 2));
};

Alerter.prototype._throttleAlert =
  function _throttleAlert(hostname, key, info) {
    var me = this;
    if (!me.throttle[hostname + key]) {
      me.throttle[hostname + key] = {
        last: Date.now()
      };
      me._send(hostname, key, info);
      console.log('sending first');
    } else {
      var now = Date.now();
      if (now > me.throttle[hostname + key].last + me.threshhold) {
        me.throttle[hostname + key].last = now;
        console.log('sending later');
        me._send(hostname, key, info);
      } else {
        // console.log('skipAlert', hostname, key, info);
      }
    }
  };

Alerter.prototype.sendAlerts = function sendAlerts(hosts) {
  var me = this;
  _.forOwn(hosts, function(keys, hostname) {
    _.keys(keys).forEach(function(key) {
      if (hosts[hostname][key].isAlert()) {
        me._throttleAlert(hostname, key, hosts[hostname][key]);
      }
    });
  });
};

module.exports = Alerter;

if (require.main === module) {
  var Mailer = require('./mailer');
  var mailer = new Mailer();
  var alerter = new Alerter(mailer, 20000);
  var hosts = {
    "consul-c1": {
      "dskAvail.1": {
        "description": "Available space on the disk",
        "max": 100,
        "value": 111,
        isAlert: function() {
          return true;
        }
      },
      "dskPercent.1": {
        "description": "Percentage of space used on disk",
        "max": 30,
        "value": 29,
        isAlert: function() {
          return false;
        }
      }
    }
  };
  setInterval(alerter.sendAlerts.bind(alerter, hosts), 300);
}