#!/usr/bin/env node

// We just apply some default values from the config
// and add the isAlert method.

var config = require('./configloader');
var _ = require('lodash');

function isEmpty(obj) {
  for (var prop in obj) {
    if (obj.hasOwnProperty(prop))
      return false;
  }
  return true;
}

if (isEmpty(config)) {
  console.error('no config. nothing to do. exiting.');
  process.exit(1)
}

function applyConfigDefaults(config, hosts) {
  _.forOwn(hosts, function(keys, hostname) {
    _.keys(keys).forEach(function(key) {
      if (!hosts[hostname][key]) {
        hosts[hostname][key] = {};
      }
      hosts[hostname][key] = _.merge(_.cloneDeep(config[key]), hosts[hostname][key]);
    });
  });
}

function addIsAlertMethod(hosts) {
  _.forOwn(hosts, function(keys, hostname) {
    _.keys(keys).forEach(function(key) {
      if (!hosts[hostname][key]) {
        hosts[hostname][key] = {};
      }
      hosts[hostname][key].isAlert = function isAlert() {
        if (this.max && this.value > this.max) {
          return true;
        }
        if (this.min && this.value < this.min) {
          return true;
        }
        return false;
      }.bind(hosts[hostname][key]);
    });
  });
}

applyConfigDefaults(config, config.hosts);

addIsAlertMethod(config.hosts);

console.log('config', JSON.stringify(config, null, 2));

module.exports = config;