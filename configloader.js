#!/usr/bin/env node

// We load the config from a single file or from all files in the
// the specified folder that have .yml extension.

var fs = require('fs');
var yaml = require('js-yaml');
var config = {};
var _ = require('lodash');

// SYS_HEALTH_CFG determines where we look for config files.
// It can be a file or a folder.
var cfgloc = process.env.SYS_HEALTH_CFG;

function loadConfigsFromDirectory(dir) {
  fs.readdirSync(dir).forEach(function(filename) {
    if (_.endsWith(filename, '.yml')) {
      _.merge(config, yaml.safeLoad(fs.readFileSync(dir + filename, 'utf8')));
    }
  });
}

// Config not specified or not found, so use the default.
function loadDefault() {
  console.log('default config', cfgloc || '');
  config = yaml.safeLoad(fs.readFileSync('config.yml', 'utf8'));
}

// See if config location is specified.
if (cfgloc) {
  try {
    var stat = fs.lstatSync(cfgloc);
  } catch (e) {
    console.log('config file not found:', cfgloc);
    throw e;
  }
  if (stat.isFile()) {
    console.log('config from file', cfgloc);
    config = yaml.safeLoad(fs.readFileSync(cfgloc, 'utf8'));
  } else if (stat.isDirectory()) {
    console.log('config from folder', cfgloc);
    loadConfigsFromDirectory(cfgloc);
  } else {
    loadDefault();
  }
} else {
  // Not specified.
  loadDefault();
}

module.exports = config;

if (require.main === module) {
  console.log(JSON.stringify(config, null, 2));
}