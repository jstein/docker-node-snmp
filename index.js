#!/usr/bin/env node

var _ = require('lodash');
var SnmpGet = require('./snmpget');
var Mailer = require('./mailer');
var Alerter = require('./alerter');
var Web = require('./web');
var config = require('./configer');

var snmpget = new SnmpGet(config.hosts, config.interval);
var mailer = new Mailer(config.mail);
var alerter = new Alerter(mailer, config.throttleMinutes);
var web = new Web(config.host, config.port, config.hosts, config.template);

var intervalID = setInterval(
  alerter.sendAlerts.bind(alerter, snmpget.hosts),
  config.interval);

process.on('SIGINT', function() {
  clearInterval(intervalID);
  web.stop();
  snmpget.stop();
  console.log('');
});