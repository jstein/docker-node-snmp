#!/usr/bin/env node

// Preconfigured mailer makes sending easy. Just call send().

var nodemailer = require('nodemailer');
var ses = require('nodemailer-ses-transport');

// config:
//   from: jstein@candoris.com
//   to:
//     - jstein@candoris.com
//     - sshrefler@candoris.com
//   accessKeyId: (defaults to process.env.AWS_ACCESS_KEY_ID)
//   secretAccessKey: (defaults to process.env.AWS_SECRET_ACCESS_KEY)
function Mailer(config) {
  this.config = config || {};
  if (!this.config.from) {
    this.config.from = 'jstein@candoris.com';
  }
  if (!this.config.to) {
    this.config.to = 'jstein@candoris.com';
  }
  this.transporter = nodemailer.createTransport(ses({
    accessKeyId: this.config.accessKeyId || process.env.AWS_ACCESS_KEY_ID,
    secretAccessKey: this.config.secretAccessKey || process.env.AWS_SECRET_ACCESS_KEY
  }));
}

Mailer.prototype.send = function send(subject, text, optcb) {
  var me = this;
  me.transporter.sendMail({
    from: me.config.from,
    to: me.config.to,
    subject: subject,
    text: text
  }, function(err, data) {
    if (err) {
      console.error('MAIL ERROR', err, 'from', me.config.from, 'to',
        me.config.to, 'subject', subject);
    }
    optcb && optcb(err, data);
  });
};

module.exports = Mailer;

if (require.main === module) {
  var mailer = new Mailer({
    from: 'jstein@candoris.com',
    to: ['jstein@candoris.com']
  });
  mailer.send('zzzzzzzzzzzzz', 'and a new test');
}