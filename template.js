var fs = require('fs');
var Hogan = require('hogan.js');

function getTemplate(stringOrFile) {
  var rv = null;
  if ('/' === stringOrFile.substr(0, 1) || './' === stringOrFile.substr(0, 2)) {
    rv = Hogan.compile(fs.readFileSync(stringOrFile, 'utf-8'));
  } else {
    rv = Hogan.compile(stringOrFile);
  }
  return rv;
}

// renders template from given string, path or URL, and data.
// caches compiled template.
function render(stringOrFile, config) {
  var template = getTemplate(stringOrFile);
  return template.render(config);
}

module.exports = {
  render: render
};