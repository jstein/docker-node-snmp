#!/usr/bin/env node

// Our job is to spit out a nice html page for those interested
// in all the lovely information we have.

var template = require('./template');
var http = require('http');
var _ = require('lodash');

function isNumeric(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}

// puts commas in if number
function format(x) {
  if (!isNumeric(x)) {
    return x;
  }
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function flatten(hosts, templateFile) {
  var holder = {
    hosts: []
  };
  _.forEach(hosts, function(objs, hostname) {
    _.forEach(objs, function(vals, objname) {
      var row = {
        host: hostname
      };
      holder.hosts.push(row);
      row.name = objname;
      row.description = vals.description;
      row.value = format(vals.value);
      row.min = format(vals.min);
      row.max = format(vals.max);
      row.number = isNumeric(vals.value);
      row.alert = vals.isAlert();
    });
  });
  return template.render(templateFile, holder);
}

function Web(host, port, hosts, templateFile) {
  this.hosts = hosts;
  this.server = http.createServer(function(req, res) {
    res.writeHead(200, {
      'Content-Type': 'text/html'
    });
    res.end(flatten(hosts, templateFile));
  }).listen(port, host);
  console.log('Server running at http://' + host + ':' + port);
  // So every connection we need to set the timeout or else when you
  // call server.close() you will be waiting forever for the sever to
  // shut down. And since this is such a critical app, we want to
  // shut down cleanly :)
  this.server.on('connection', function(socket) {
    socket.setTimeout(10000);
  });
}

Web.prototype.stop = function stop() {
  this.server.close();
};

module.exports = Web;


if (require.main === module) {
  var web = new Web(null, 8080, {
    "consul-c1": {
      "sysName.0": {
        "description": "System name",
        isAlert: function() {
          return Math.random() < .5
        }
      },
      "dskAvail.1": {
        "description": "Available space on the disk",
        isAlert: function() {
          return Math.random() < .5
        }
      },
      "dskPercent.1": {
        "description": "Percentage of space used on disk",
        "max": 50,
        "min": 35,
        value: 39,
        isAlert: function() {
          return Math.random() < .5
        }
      }
    },
    "consul-c2": {
      "sysName.0": {
        "description": "System name",
        value: 'my system',
        isAlert: function() {
          return Math.random() < .5
        }
      },
      "dskAvail.1": {
        "description": "Available space on the disk",
        "min": 5351576339,
        isAlert: function() {
          return Math.random() < .5
        }
      },
      "dskPercent.1": {
        "description": "Percentage of space used on disk",
        "max": 70,
        isAlert: function() {
          return Math.random() < .5
        }
      }
    }
  }, './index.tmp');
  process.on('SIGINT', function() {
    web.stop();
    console.log('');
  });
}